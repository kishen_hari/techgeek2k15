****TECH GEEK****

For those who want a new challenge everyday, we have just the thing for you! 


****EVENT FORMAT****

This is an online coding event that will enable a programmer to show off his language versatility and his coding skills.This is going to be an event to test how big of a computer geek you are. This event will test skills from coding to debugging, and from to technical knowledge to reverse coding. Tech Geek will consist of a set of tasks and questions to be done in a given amount of tim. There can be only one winner, so the fastest Person to complete the task will be given rewards. The questions will be related to Computer Science.

Number of Levels: 2

Everyday at a fixed time, from 8 to 10 pm, a question or a set of questions will be posted on the tech geek site. The first user to correctly complete the given task will get a 200 Rs worth mobile recharge.

A live leaderboard will be maintained so that the participant can check his/her ranking.
The program submitted by the participant will be considered correct if it gives the expected output for all test cases

Prize pool: 8K + Gift Vouchers

**RULES**

● This is strictly an individual online event.

● The duration of the event is 2 weeks

● Registration is required for participation and there should be no duplicate accounts.

● There will be a total of 30 questions.

● Different levels will carry different weightage of points according to difficulty.

● If any malicious activity like submitting the duplicate code is detected the user account will be deleted without prior notice.

● Professionals are NOT allowed to participate

● Attacking or flooding the server will lead to disqualification.

● Multiple accounts from a participant are NOT allowed.

● Participants suspected of using unfair means WILL BE disqualified.

● Any misuse of the Hackmaster forum will lead to immediate disqualification.

● The decisions and judgement of the coordinators will be final.

● Rules are subject to change at any point in time. 

**CONTACTS**

Marc Cyrus
Ph: +91-9995438933