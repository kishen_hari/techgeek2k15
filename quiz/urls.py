from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^signup$', views.signup, name='signup'),
    url(r'^dashboard$', views.dashboard, name='dashboard'),
    url(r'^signin$', views.login, name='signin'),
    url(r'^startchallenge$', views.startchallenge, name='startchallenge'),
    url(r'^submitchallenge1$', views.submitchallenge1, name='submitchallenge1'),
    url(r'^logout$', views.logout, name='logout'),
    url(r'^leaderboard$',views.leaderboard, name='leaderboard'),
    url(r'^about$',views.about,name='about'),
]
