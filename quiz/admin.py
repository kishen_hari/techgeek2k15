from django.contrib import admin
from quiz.models import Player
from quiz.models import Question

admin.site.register(Player)
admin.site.register(Question)

# Register your models here.
