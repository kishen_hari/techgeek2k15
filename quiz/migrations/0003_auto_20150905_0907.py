# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('quiz', '0002_auto_20150905_0858'),
    ]

    operations = [
        migrations.AlterField(
            model_name='player',
            name='start_time',
            field=models.DateTimeField(null=True),
        ),
    ]
