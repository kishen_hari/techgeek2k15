# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('quiz', '0005_auto_20150905_1420'),
    ]

    operations = [
        migrations.AddField(
            model_name='player',
            name='challenge_complete',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='player',
            name='start_time',
            field=models.DateTimeField(default=datetime.datetime(2015, 9, 5, 22, 49, 40, 957866)),
        ),
    ]
