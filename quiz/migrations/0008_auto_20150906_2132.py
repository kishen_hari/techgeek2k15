# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('quiz', '0007_auto_20150906_1046'),
    ]

    operations = [
        migrations.AlterField(
            model_name='player',
            name='start_time',
            field=models.DateTimeField(default=datetime.datetime(2015, 9, 6, 21, 32, 16, 840106)),
        ),
    ]
