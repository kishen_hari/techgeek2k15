# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('quiz', '0003_auto_20150905_0907'),
    ]

    operations = [
        migrations.AlterField(
            model_name='player',
            name='start_time',
            field=models.DateTimeField(default=datetime.datetime(2015, 9, 5, 9, 11, 23, 559308)),
        ),
    ]
