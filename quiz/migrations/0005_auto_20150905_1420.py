# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('quiz', '0004_auto_20150905_0911'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='question',
            name='questionid',
        ),
        migrations.AlterField(
            model_name='player',
            name='start_time',
            field=models.DateTimeField(default=datetime.datetime(2015, 9, 5, 14, 20, 45, 112212)),
        ),
    ]
