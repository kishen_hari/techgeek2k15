# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Player',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('email', models.CharField(default=b'N/A', max_length=50)),
                ('college', models.CharField(default=b'N/A', max_length=50)),
                ('mob', models.CharField(max_length=12)),
                ('start_time', models.DateTimeField()),
                ('totalscore', models.IntegerField(default=0)),
                ('userid', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Question',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True)),
                ('question', models.CharField(default=b'', max_length=5000)),
                ('option1', models.CharField(default=b'N/A', max_length=50)),
                ('option2', models.CharField(default=b'N/A', max_length=50)),
                ('option3', models.CharField(default=b'N/A', max_length=50)),
                ('option4', models.CharField(default=b'N/A', max_length=50)),
                ('answer', models.IntegerField(default=0)),
                ('questionid', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
