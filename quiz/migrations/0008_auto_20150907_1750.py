# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('quiz', '0007_auto_20150906_1046'),
    ]

    operations = [
        migrations.AlterField(
            model_name='player',
            name='start_time',
            field=models.DateTimeField(default=datetime.datetime(2015, 9, 7, 17, 50, 32, 226949)),
        ),
        migrations.AlterField(
            model_name='question',
            name='option1',
            field=models.CharField(default=b'N/A', max_length=100),
        ),
        migrations.AlterField(
            model_name='question',
            name='option2',
            field=models.CharField(default=b'N/A', max_length=100),
        ),
        migrations.AlterField(
            model_name='question',
            name='option3',
            field=models.CharField(default=b'N/A', max_length=100),
        ),
        migrations.AlterField(
            model_name='question',
            name='option4',
            field=models.CharField(default=b'N/A', max_length=100),
        ),
        migrations.AlterField(
            model_name='question',
            name='question',
            field=models.CharField(default=b'', max_length=500),
        ),
    ]
