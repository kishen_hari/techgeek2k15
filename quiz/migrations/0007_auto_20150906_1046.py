# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('quiz', '0006_auto_20150905_2249'),
    ]

    operations = [
        migrations.AddField(
            model_name='player',
            name='challenge_started',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='player',
            name='start_time',
            field=models.DateTimeField(default=datetime.datetime(2015, 9, 6, 10, 46, 27, 569133)),
        ),
    ]
