from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render_to_response
from django.core.context_processors import csrf
from django.http import HttpResponseRedirect, HttpResponse
from django.template import RequestContext,loader
from player import *
from question import *
from pytz import timezone
import datetime
from techgeek import settings

# Create your views here.
def dashboard(request):
    context=RequestContext(request)
    template=loader.get_template('dashboard.html')
    if 'playerid' in request.session:
        return render_to_response('dashboard.html',{'name':Player.objects.get(id=request.session.get('playerid','')).name,'rank':getRank(request.session['playerid']),'score':Player.objects.get(id=request.session['playerid']).totalscore,})
    errors=[]
    errors.append('You are not logged in')
    return render_to_response('index.html',{'errors':errors,})

def index(request):
    context=RequestContext(request)
    template=loader.get_template('index.html')
    return HttpResponse(template.render(context))

@csrf_exempt
def signup(request):
    errors = []
    print request.POST
    if request.method == 'POST':
        if not request.POST.get('username', ''):
            errors.append('Enter username.')
        if not request.POST.get('password', ''):
            errors.append('Enter a password.')
        if not request.POST.get('name', ''):
            errors.append('Enter a name.')
        if not request.POST.get('email', ''):
            errors.append('Enter an email.')
        if not request.POST.get('mob', ''):
            errors.append('Enter a mob.')
        if not request.POST.get('college', ''):
            errors.append('Enter a college.')
        if request.POST.get('password'):
            if request.POST.get('password', 'one') != request.POST.get('confirmpassword', 'two'):
                errors.append('Passwords do not match.')
        if User.objects.filter(username=request.POST.get('username', '')).exists():
            errors.append('Username Not Available')

        if not errors:
            if register_player(request):
                request.session['playerid'] = get_player_id(request.user.id)
                return HttpResponseRedirect('dashboard', "Registration Successful")
            else:
                errors.append('Error, Try again')
    c = {'errors': errors}
    c.update(csrf(request))
    return render_to_response('signup.html',c)


@csrf_exempt
def login(request, msg=""):
    errors = [msg]
    print request.POST
    if request.method == 'POST':
        if not request.POST.get('username', ''):
            errors.append('Enter username.')
        if not request.POST.get('password', ''):
            errors.append('Enter a password.')
        if errors == ['']:
            if authenticate_user(request):
                request.session['playerid'] = get_player_id(request.user.id)
                if(request.session['playerid']):
                    return HttpResponseRedirect('dashboard')
            else:
                if errors == ['']:
                    errors.append('Invalid Credentials')
    c = {'errors': errors}
    c.update(csrf(request))
    return render_to_response("login.html", c)

@csrf_exempt
def startchallenge(request):
    if not 'playerid' in request.session:
        errors=[]
        errors.append('You are not logged in')
        return render_to_response('signin.html',{'errors':errors,})

    start_time=datetime.time(hour=00,minute=0,second=0)
    end_time=datetime.time(hour=22,minute=0,second=0)
    timeup=True
    if(start_time<=datetime.datetime.now().time()<=end_time):
        player=Player.objects.get(id=request.session['playerid'])
        if player.challenge_complete==True:
            return render_to_response('questions.html',{'submitted':True,})
        if not player.challenge_started:
            player.start_time = datetime.datetime.now()
            player.challenge_started = True

        player.save()
        questions = generateQuestions()
        return render_to_response('round1.html',{'questions':questions,'name':Player.objects.get(id=request.session['playerid']).name,})
    return render_to_response('questions.html',{'timeup': timeup,'name': Player.objects.get(id=request.session['playerid']).name,})
@csrf_exempt
def submitchallenge1(request):
    end_time=datetime.time(hour=22,minute=0,second=0)
    if(datetime.datetime.now().time()>end_time):
        return render_to_response('questions.html',{'timeup':True,'name':Player.objects.get(id=request.session['playerid']).name,})

    player=Player.objects.get(id=request.session['playerid'])
    player.totalscore+=evalQuestions(request) * 10
    print evalQuestions(request)
    timez=timezone(settings.TIME_ZONE)
    start=player.start_time.astimezone(timez)
    player_start=start.hour*3600+start.minute*60+start.second
    end=datetime.datetime.now().time()
    player_end=end.hour*3600+end.minute*60+end.second
    player.totalscore+=float((7200-(player_end-player_start))/100)
    player.challenge_complete=True
    player.save()
    return render_to_response('questions.html')

def leaderboard(request):
    return render_to_response('leaderboard.html',{'name': Player.objects.get(id=request.session['playerid']).name,'leaderboard':getLeaderboard(),})

def about(request):
    return render_to_response('info.html')

def logout(request):
    logout_player(request)
    return HttpResponseRedirect('signin')
