import random
from quiz.models import Question

def generateQuestions():
    questions=[]
    sample=random.sample(range(1,101),20)
    for i in sample:
        q=Question.objects.get(id=i)
        questions.append(q)

    return questions

def evalQuestions(request):
    total=0
    for i in range(1,21):
        current=0
        current=request.POST['question'+str(i)]
        question=Question.objects.get(id=current)
        if ('answer' + str(i)) in request.POST:
            print request.POST['answer'+str(i)]+str(question.answer)
            if str(request.POST['answer'+str(i)]) == str(question.answer):
                total+=1

    return total

