from django.db import models
from django.contrib.auth.models import User
import datetime

# Create your models here.

class Player(models.Model):
    id = models.AutoField(primary_key=True)
    userid = models.ForeignKey(User)
    name = models.CharField(max_length=50)
    email = models.CharField(max_length=50,default='N/A')
    college = models.CharField(max_length=50,default='N/A')
    mob = models.CharField(max_length=12)
    start_time=models.DateTimeField(default=datetime.datetime.now())
    challenge_started=models.BooleanField(default=False)
    challenge_complete=models.BooleanField(default=False)
    totalscore = models.IntegerField(default=0)
    
    def __unicode__(self):
    	return self.name+" Name: "+str(self.name)

class Question(models.Model):
    id = models.AutoField(primary_key=True)
    question = models.CharField(max_length=500,default='')
    option1 = models.CharField(max_length=100,default='N/A')
    option2 = models.CharField(max_length=100,default='N/A')
    option3 = models.CharField(max_length=100,default='N/A')
    option4 = models.CharField(max_length=100,default='N/A')
    answer = models.IntegerField(default=0)
    

