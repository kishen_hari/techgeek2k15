from django.contrib.auth.models import User
from django.contrib.auth import authenticate, logout, login
from quiz.models import Player
import os


def register_player(request):
    data=request.POST
    DIR = os.path.dirname(os.path.dirname(__file__)) + '/media'
    user = User(username=data['username'])
    user.first_name = data['name']
    user.set_password(data['password'])
    user.email=data['email']
    try:
        user.save()
    except Exception as e:
        print '%s (%s)' % (e.message, type(e))
        return False
    if True:
        p = Player(userid=user, name=data['name'], email=data['email'], college=data['college'], mob=data['mob'])
        p.save()
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
        print user
        newpath = DIR + '/' + str(user.username.replace(" ", "")) + '_' + str(user.id)
        if not os.path.exists(newpath): os.makedirs(newpath)
        print "Success"

        return True
    else:
        try:
            user.delete()
            print "both"
            return False
        except:
            try:
                user.delete()
                print "user"
                return False
            except:
                return False


def authenticate_user(request):
    username = request.POST.get('username','')
    password = request.POST.get('password','')
    user = authenticate(username=username, password=password)
    print user
    if user is not None:
        if user.is_active:
            login(request, user)
            return True
        else:
            return False
    else:
        return False


def logout_player(request):
    if request.session.get('playerid', '') != '':
        del request.session['playerid']
    logout(request)


def get_player_id(userid):
    try:
        p = Player.objects.get(userid=userid)
        return p.id
    except Player.DoesNotExist:
        return
def getLeaderboard():
    leaderboard=Player.objects.all().order_by("-totalscore")
    return leaderboard

def getRank(userid):
    leaderboard=Player.objects.all().order_by("-totalscore")
    k=1
    for i in leaderboard:
        if(i.id==userid):
            return k
        k+=1
    return -1